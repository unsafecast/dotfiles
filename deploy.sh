#!/bin/sh

# Install Vim Plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# Copy nvim config and install all the plugins
mkdir -p ~/.config/nvim
cp ./nvim/init.vim ~/.config/nvim/init.vim
nvim -c "PlugInstall" -c "qa"

# Copy fish config
mkdir -p ~/.config/fish/
cp ./fish/config.fish ~/.config/fish/config.fish

# Copy alacritty config
mkdir -p ~/.config/alacritty
cp ./alacritty/alacritty.yml ~/.config/alacritty/alacritty.yml

# Copy kitty config
mkdir -p ~/.config/kitty
cp ./kitty/* ~/.config/kitty/

# Copy kakoune config
mkdir -p ~/.config/kak
cp ./kak/* ~/.config/kak/
