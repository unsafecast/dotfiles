fish_add_path /home/bit/.local/bin

alias ll "exa -l"
alias nvw "nvim -c Goyo"
alias gc "git commit"
alias gp "git push"
alias gs "git status"

set BAT_THEME "gruvbox-dark"
export BAT_THEME
