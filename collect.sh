#!/bin/sh

cp ~/.config/nvim/init.vim ./nvim/init.vim

cp ~/.config/fish/config.fish ./fish/config.fish

cp ~/.config/alacritty/alacritty.yml ./alacritty/alacritty.yml
cp ~/.config/alacritty/gruvbox.yml ./alacritty/gruvbox.yml

cp ~/.config/kitty/kitty.conf ./kitty/kitty.conf

cp ~/.config/kak/kakrc ./kak/kakrc
