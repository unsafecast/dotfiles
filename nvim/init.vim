call plug#begin("~/.config/nvim/plug")
    Plug 'ziglang/zig.vim'
    Plug 'dag/vim-fish'
    Plug 'cloudhead/neovim-fuzzy'
    Plug 'octol/vim-cpp-enhanced-highlight'
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    Plug 'junegunn/goyo.vim'
    Plug 'andreasvc/vim-256noir'
    Plug 'srcery-colors/srcery-vim'
call plug#end()


" ------------------------------ "
" Color Scheme                   "
" ------------------------------ "

set termguicolors

set background="dark"
colorscheme srcery


" ------------------------------ "
" UI Elements                    "
" ------------------------------ "

set relativenumber
set number
set cursorline


" ------------------------------ "
" Font & Spacing                 "
" ------------------------------ "

set tabstop=4
set shiftwidth=4
set expandtab


" ------------------------------ "
" Key Bindings                   "
" ------------------------------ "

nnoremap <C-p> :FuzzyOpen<CR>
nnoremap m     :noh<CR>

" ------------------------------ "
" C & C++ Highlighting           "
" ------------------------------ "

let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_posix_standard = 1
let g:cpp_experimental_simple_template_highlight = 1
let g:cpp_concepts_highlight = 1

